package com.egindri.calculadora.service;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.egindri.calculadora.funcs.TipoCalculo;

@Service
public class CalculadoraService {

	public BigDecimal calc(TipoCalculo tipo, BigDecimal a, BigDecimal b) {
		return tipo.getFuncInterface().calc(a, b);
	}
}