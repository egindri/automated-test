package com.egindri.calculadora.funcs;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Divisor implements FuncInterface {

	public BigDecimal calc(BigDecimal a, BigDecimal b) {
		return a.divide(b, 2, RoundingMode.HALF_UP);
	}
}
