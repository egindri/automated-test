package com.egindri.calculadora.funcs;

import java.math.BigDecimal;

public interface FuncInterface {

	public BigDecimal calc(BigDecimal a, BigDecimal b);
}
