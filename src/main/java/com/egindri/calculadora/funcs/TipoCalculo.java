package com.egindri.calculadora.funcs;

public enum TipoCalculo {

	SOMA(new Somador()), 
	SUBTRACAO(new Subtrator()), 
	MULTIPLICACAO(new Multiplicador()),
	DIVISAO(new Divisor());
	
	private FuncInterface funcInterface;
	
	private TipoCalculo(FuncInterface funcInterface) {
		this.funcInterface = funcInterface;
	}
	
	public FuncInterface getFuncInterface() {
		return funcInterface;
	}
}