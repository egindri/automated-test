package com.egindri.calculadora.funcs;

import java.math.BigDecimal;

public class Subtrator implements FuncInterface {

	public BigDecimal calc(BigDecimal a, BigDecimal b) {
		return a.subtract(b);
	}
}