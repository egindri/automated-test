package com.egindri.calculadora.funcs;

import java.math.BigDecimal;

public class Somador implements FuncInterface {

	public BigDecimal calc(BigDecimal a, BigDecimal b) {
		return a.add(b);
	}
}
