package com.egindri.calculadora.funcs;

import java.math.BigDecimal;

public class Multiplicador implements FuncInterface {

	public BigDecimal calc(BigDecimal a, BigDecimal b) {
		return a.multiply(b);
	}
}
