package com.egindri.calculadora.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.egindri.calculadora.funcs.TipoCalculo;
import com.egindri.calculadora.service.CalculadoraService;

@Controller
public class CalculadoraController {
	
	@Autowired
	private CalculadoraService service;

	@GetMapping("/{tipo}/{a}/{b}")
	@CrossOrigin
	public ResponseEntity<BigDecimal> calc(@PathVariable TipoCalculo tipo, 
											@PathVariable BigDecimal a, 
											@PathVariable BigDecimal b) {
		BigDecimal result = service.calc(tipo, a, b);

		return ResponseEntity.ok().body(result);
	}
}