package com.egindri.calculadora.integration;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import com.egindri.calculadora.CalculadoraApplicationTest;
import com.egindri.calculadora.funcs.TipoCalculo;

public class CalculoIntegrationTest extends CalculadoraApplicationTest {

	@Test
	public void testSomador() {
		String tipo = TipoCalculo.SOMA.toString();
		BigDecimal a = new BigDecimal(50);
		BigDecimal b = new BigDecimal(100);
		
		String url = createBaseUrl() + tipo + "/" + a + "/" + b;
		HttpMethod method = HttpMethod.GET;
		HttpEntity<Object> header = buildDefaultHeader();
		ResponseEntity<BigDecimal> response = testRestTemplate.exchange(url, method, header, BigDecimal.class);

		Assert.assertTrue(response.getBody().compareTo(new BigDecimal(150)) == 0);
	}

	@Test
	public void testMultiplicador() {
		String tipo = TipoCalculo.MULTIPLICACAO.toString();
		BigDecimal a = new BigDecimal(50);
		BigDecimal b = new BigDecimal(100);
		
		String url = createBaseUrl() + tipo + "/" + a + "/" + b;
		HttpMethod method = HttpMethod.GET;
		HttpEntity<Object> header = buildDefaultHeader();
		ResponseEntity<BigDecimal> response = testRestTemplate.exchange(url, method, header, BigDecimal.class);

		Assert.assertTrue(response.getBody().compareTo(new BigDecimal(5000)) == 0);
	}

	@Test
	public void testDivisor() {
		String tipo = TipoCalculo.DIVISAO.toString();
		BigDecimal a = new BigDecimal(100);
		BigDecimal b = new BigDecimal(50);
		
		String url = createBaseUrl() + tipo + "/" + a + "/" + b;
		HttpMethod method = HttpMethod.GET;
		HttpEntity<Object> header = buildDefaultHeader();
		ResponseEntity<BigDecimal> response = testRestTemplate.exchange(url, method, header, BigDecimal.class);

		Assert.assertTrue(response.getBody().compareTo(new BigDecimal(2)) == 0);
	}

	@Test
	public void testSubtrator() {
		String tipo = TipoCalculo.SUBTRACAO.toString();
		BigDecimal a = new BigDecimal(100);
		BigDecimal b = new BigDecimal(50);
		
		String url = createBaseUrl() + tipo + "/" + a + "/" + b;
		HttpMethod method = HttpMethod.GET;
		HttpEntity<Object> header = buildDefaultHeader();
		ResponseEntity<BigDecimal> response = testRestTemplate.exchange(url, method, header, BigDecimal.class);

		Assert.assertTrue(response.getBody().compareTo(new BigDecimal(50)) == 0);
	}
}
