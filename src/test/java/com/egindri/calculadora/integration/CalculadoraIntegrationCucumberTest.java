package com.egindri.calculadora.integration;

import org.junit.runner.RunWith;

import com.egindri.calculadora.CalculadoraApplicationTest;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/")
public class CalculadoraIntegrationCucumberTest extends CalculadoraApplicationTest {}
