package com.egindri.calculadora.integration;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class CalculadoraIntegrationCucumberStepDefinitions extends CalculadoraIntegrationCucumberTest {
	
	private List<BigDecimal> values = new ArrayList<>();
	private BigDecimal result;
	

	@Dado("^que a aplicação está ativa$")
    public void applicationActive() throws Exception {
		
		String url = createBaseUrl() + "actuator/health";
		HttpMethod method = HttpMethod.GET;
		HttpEntity<Object> header = buildDefaultHeader();
		
		ResponseEntity<Object> response = testRestTemplate.exchange(url, method, header, Object.class);
		
		Assert.assertEquals(response.getStatusCodeValue(), 200);
    }

    @Quando("^insiro o valor (.*)$")
    public void insertValue(BigDecimal value) {
    	values.add(value);
    }
    
    @E("^escolho (.*)$")
    public void chooseFunction(String function) {
    	String url = createBaseUrl() + function.toUpperCase() + values
    																.stream()
    																.map(v -> "/" + v.toString())
    																.collect(Collectors.joining());
    	HttpMethod method = HttpMethod.GET;
    	HttpEntity<Object> header = buildDefaultHeader();
    	
    	ResponseEntity<BigDecimal> response = testRestTemplate.exchange(url, method, header, BigDecimal.class);

    	result = response.getBody();
    }
    
    @Entao("^confirmo que é apresentado o resultado (.*)$")
    public void confirmResult(BigDecimal value) {
    	Assert.assertTrue(result.compareTo(value) == 0);
    	
    	values.clear();
    	result = null;
    }
}