package com.egindri.calculadora;

import java.util.concurrent.TimeUnit;

import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DirtiesContext
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public abstract class CalculadoraApplicationTest {

	private final static String URL = "http://localhost:8081";

	
	@LocalServerPort
	private int port;

	@Value("${chromedriver.path}")
	private String chromeDriverPath;
	
	protected TestRestTemplate testRestTemplate = new TestRestTemplate();
	
	protected WebDriver driver;

	protected String createBaseUrl() {
		return "http://localhost:" + port + "/";
	}
	
	protected HttpEntity<Object> buildDefaultHeader() {
		return new HttpEntity<>(null, new HttpHeaders());
	}
	
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver", chromeDriverPath);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	}

	protected String getUrl() {
	    return URL + "/calculadora.html";
	}
}