package com.egindri.calculadora.functional;

import java.math.BigDecimal;
import java.util.function.Function;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.egindri.calculadora.CalculadoraApplicationTest;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class CalculadoraFunctionalCucumberStepDefinitions extends CalculadoraApplicationTest {
	
	@Dado("^que a aplicação está ativa$")
    public void applicationActive() throws Exception {
		setUp();
		driver.get(getUrl());
    }

    @Quando("^insiro o valor (.*)$")
    public void insertValue(BigDecimal value) {
    	WebElement fieldA = driver.findElement(By.id("a"));
		if (fieldA.getAttribute("value").isEmpty()) {
    		fieldA.sendKeys(value.toString());
    	} else {
    		driver.findElement(By.id("b")).sendKeys(value.toString());
    	}
    }
    
    @E("^escolho (.*)$")
    public void chooseFunction(String function) {
    	driver.findElement(By.xpath("//button[contains(., '" + function.toUpperCase() + "')]")).click();
    }
    
    @Entao("^confirmo que é apresentado o resultado (.*)$")
    public void confirmResult(BigDecimal value) {
//    	String result = driver.findElement(By.id("result")).getText();
    	WebDriverWait wait = new WebDriverWait(driver, 10);
		
    	WebElement element = wait.until(new Function<WebDriver, WebElement>() {
											@Override
											public WebElement apply(WebDriver driver) {
												WebElement element = driver.findElement(By.id("result"));
												return element.getText().isEmpty() ? null : element; 
											}
								    	});
    	
    	String result = element.getText();
		Assert.assertTrue(new BigDecimal(result).compareTo(value) == 0);

		driver.close();
    }
}