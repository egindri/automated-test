package com.egindri.calculadora.funcs;

import java.math.BigDecimal;

import org.junit.Test;

import org.junit.Assert;

public class SubtratorTest {
	
	private Subtrator subtrator = new Subtrator();
	
	@Test
	public void subtracaoTest() {
		BigDecimal a = new BigDecimal(50);
		BigDecimal b = new BigDecimal(10);
		
		BigDecimal result = subtrator.calc(a, b);
		
		Assert.assertEquals(new BigDecimal(40), result);
	}
}
