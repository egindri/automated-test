package com.egindri.calculadora.funcs;

import java.math.BigDecimal;

import org.junit.Test;

import org.junit.Assert;

public class SomadorTest {
	
	private Somador somador = new Somador();
	
	@Test
	public void somaTest() {
		BigDecimal a = new BigDecimal(50);
		BigDecimal b = new BigDecimal(10);
		
		BigDecimal result = somador.calc(a, b);
		
		Assert.assertEquals(new BigDecimal(60), result);
	}
}
