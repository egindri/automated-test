package com.egindri.calculadora.funcs;

import java.math.BigDecimal;

import org.junit.Test;

import org.junit.Assert;

public class MultiplicadorTest {
	
	private Multiplicador multiplicador = new Multiplicador();
	
	@Test
	public void multiplicacaoTest() {
		BigDecimal a = new BigDecimal(50);
		BigDecimal b = new BigDecimal(10);
		
		BigDecimal result = multiplicador.calc(a, b);
		
		Assert.assertEquals(new BigDecimal(500), result);
	}
}
