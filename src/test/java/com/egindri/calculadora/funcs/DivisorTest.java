package com.egindri.calculadora.funcs;

import java.math.BigDecimal;

import org.junit.Test;

import org.junit.Assert;

public class DivisorTest {
	
	private Divisor divisor = new Divisor();
	
	@Test
	public void divisaoTest() {
		BigDecimal a = new BigDecimal(50);
		BigDecimal b = new BigDecimal(10);
		
		BigDecimal result = divisor.calc(a, b);
		
		Assert.assertEquals(new BigDecimal(5).setScale(2), result);
	}
}
