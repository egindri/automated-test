# language: pt
Funcionalidade: Calcular

	Cenário: Somar
		Dado que a aplicação está ativa
		Quando insiro o valor 5
		E insiro o valor 10
		E escolho Soma
		Então confirmo que é apresentado o resultado 15

	Cenário: Multiplicar
		Dado que a aplicação está ativa
		Quando insiro o valor 5
		E insiro o valor 10
		E escolho Multiplicacao
		Então confirmo que é apresentado o resultado 50

	Cenário: Subtrair
		Dado que a aplicação está ativa
		Quando insiro o valor 10
		E insiro o valor 5
		E escolho Subtracao
		Então confirmo que é apresentado o resultado 5 

	Cenário: Dividir
		Dado que a aplicação está ativa
		Quando insiro o valor 10
		E insiro o valor 5
		E escolho Divisao
		Então confirmo que é apresentado o resultado 2